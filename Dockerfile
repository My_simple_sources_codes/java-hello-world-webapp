FROM tomcat:8.5.47-jdk8-openjdk
  
COPY target/java-hello-world.war /usr/local/tomcat/webapps
